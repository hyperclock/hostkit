<?php
require_once "../conf_inc.php";
require_once "../errors_inc.php";

session_start();
session_cache_limiter('nocache');

error_reporting($error_reporting);

import_request_variables('p', 'p_');

require_once "../check_correct.php";

if($_SESSION['login'] === "yes") {
    ($GLOBALS["___mysqli_ston"] = mysqli_connect($hostname,  $admin,  $password_sql)) or die($error_connectdb);
    mysqli_select_db($GLOBALS["___mysqli_ston"], $database) or die($error_selectdb);


    $query = "select db, password from users where user='$p_user'";
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query) or die($error_select);

    $row = mysqli_fetch_array($result);

    if($row['db'] === "on" && $p_db == "") {
        mysqli_select_db($GLOBALS["___mysqli_ston"], mysql) or die($error_selectdb);

        $query = "delete from user where User='$p_user'";
        mysqli_query($GLOBALS["___mysqli_ston"], $query) or die($error_delete);

        $query = "FLUSH PRIVILEGES;";
        mysqli_query($GLOBALS["___mysqli_ston"], $query) or die("Cant FLUSH PRIVILEGES");

    }


    if($row['db'] === "" && $p_db == "on") {
        mysqli_select_db($GLOBALS["___mysqli_ston"], mysql) or die($error_selectdb);

        $query = "GRANT USAGE ON *.* TO $p_user@localhost IDENTIFIED BY '$row[password]';";
        mysqli_query($GLOBALS["___mysqli_ston"], $query) or die("Cant create user $p_user");

        $query = "FLUSH PRIVILEGES;";
        mysqli_query($GLOBALS["___mysqli_ston"], $query) or die("Cant FLUSH PRIVILEGES");

    }

    mysqli_select_db($GLOBALS["___mysqli_ston"], $database) or die($error_selectdb);

    $query = "update users  set db='$p_db', db_expday='$p_day', db_expmonth='$p_month', db_expyear='$p_year' where user='$p_user'";
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query) or die($error_update);

}

header("Location:change_properties.php?user=$p_user");
?>

