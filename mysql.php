<?php
require_once './conf_inc.php';
require_once './i18n.php';
require_once './errors_inc.php';

session_start();
session_cache_limiter('nocache');

if(IsSet($_SESSION['user'])) {

    error_reporting($error_reporting);

    @($GLOBALS["___mysqli_ston"] = mysqli_connect($hostname,  $admin,  $password_sql)) or die($error_connectdb);
    @mysqli_select_db($GLOBALS["___mysqli_ston"], $database) or die($error_selectdb);


    $query = "select ID, db from users where user='$_SESSION[user]'";
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query) or die($error_select);

    $row = mysqli_fetch_array($result);

    $user = $_SESSION['user'];

    if($row['db'] === "on") {

        import_request_variables('p', 'p_');
        import_request_variables('g', 'g_');

        $query = "select ID from domains where user_id='$row[ID]'";
        $result = mysqli_query($GLOBALS["___mysqli_ston"], $query) or die($error_select);

        $num_db = $mysql_num_db*mysqli_num_rows($result);

        if(IsSet($p_database)) {

            mysqli_select_db($GLOBALS["___mysqli_ston"], mysql) or die($error_selectdb);

            $query = "select Db from db where User='$user'";
            $result = mysqli_query($GLOBALS["___mysqli_ston"], $query) or die($error_select);

            $num_created_db = mysqli_num_rows($result);

            if($num_created_db < $num_db) {
            
                $password = $_SESSION['pass'];

                $p_database = $user . "_" . $p_database;

                ((is_null($___mysqli_res = mysqli_query($GLOBALS["___mysqli_ston"], "CREATE DATABASE $p_database"))) ? false : $___mysqli_res) or die(_("Database with that name already exist. Can't create database."));

                $query = "GRANT ALL PRIVILEGES ON $p_database.* TO $user@localhost IDENTIFIED BY '$password';";

                mysqli_query($GLOBALS["___mysqli_ston"], $query) or die("Can't create new user");

                $query = "FLUSH PRIVILEGES;";
                mysqli_query($GLOBALS["___mysqli_ston"], $query) or die("Cant FLUSH PRIVILEGES");

                $db_created = _("Database") . " $p_database ". _("created") . ". <br /> <br />";

            } else {
                $limit_reached = _("You can't create more than") . " $num_db " . _("databases") . ".";
            }
        }
        
        if(IsSet($g_drop_db)) {
        
            mysqli_select_db($GLOBALS["___mysqli_ston"], mysql) or die($error_selectdb);

            $query = " DROP DATABASE $g_drop_db";
            mysqli_query($GLOBALS["___mysqli_ston"], $query) or die("Cant drop database");


            $query = "delete from db where Db='$g_drop_db' and User='$user'";
            mysqli_query($GLOBALS["___mysqli_ston"], $query) or die($error_delete);

            $query = "FLUSH PRIVILEGES;";
            mysqli_query($GLOBALS["___mysqli_ston"], $query) or die("Cant FLUSH PRIVILEGES");

        }
        
        echo("<?xml version=\"1.0\" encoding=\"$charset\"?>");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="<?php echo($lang); ?>" xml:lang="<?php echo($lang); ?>" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo _("mysql") ?></title>
<meta http-equiv="Content-type" content="text/html; charset=<?php echo($charset); ?>" />
<link rel="stylesheet" type="text/css" href="css/<?php echo($stylesheet); ?>/style.css" />
</head>
<body>
<div>
<?php
include_once './templates/header.php';
?>
<br />
<?php echo _("You can create"); ?> <?php echo("$num_db " . _("databases") . ". <br /><br />"); ?>
<?php echo("$db_created $limit_reached <br /><br />"); ?>
<?php echo _("Create database"); ?>:
<form name="form1" action="mysql.php" method="post" accept-charset="ISO-8859-1">             
<?php echo($user); ?>_<input name="database" size="20"> 
<input type="hidden" name="vari" value="vv">
<input type="submit" value="<?php echo _("Create"); ?>">
</form>
<br />
<p>
<?php


        mysqli_select_db($GLOBALS["___mysqli_ston"], mysql) or die($error_selectdb);

        $query = "select Db from db where User='$user'";
        $result_db = mysqli_query($GLOBALS["___mysqli_ston"], $query) or die($error_select);

        while($row_db = mysqli_fetch_array($result_db)) {
            
            echo("&nbsp;&nbsp; <font class=domain> " . _("database") . ":  &nbsp; </font>
            $row_db[Db] &nbsp;&nbsp;&nbsp; <a href=\"mysql.php?drop_db=$row_db[Db]\"
            onclick=\"if(confirm('" . _("Are you sure you want to drop database") . " $row_db[Db]?')) return true; else return false;\">" . _("Drop") . "</a><br />");
        }

    } else {
        require_once './templates/error_mysql.tpl';
    }


} else {
    header("Location:login.php");
    exit;
}
?>
</p>
<br />
<a href="<?php echo($phpmyadmin); ?>" target="_blank">
<?php echo _("Manage databases with phpMyAdmin"); ?> </a>
<?php
include_once './templates/footer.php';
?>
</div>
</body>
</html>
