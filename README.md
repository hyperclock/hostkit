HostKit
=======

_**<a href="https://osp.hostkit.eu" target="_blank">HostKit</a>**_ is based on a very old piece of opensource software called [*"Web Hosting Tool"*](https://sourceforge.net/projects/wht/), also [*"WHT"*](https://sourceforge.net/projects/wht/) is known in it's short form. This was very good at the time, but also only got it run on Fedora. I prefer Debian, so I decided to start with Debian 10 as the devolpment OS. I also want this to be a bit more universal, such as:
* All [Debian](https://www.debian.org/) Derivates
* [CentOS](https://www.centos.org/), include [Fedora](https://getfedora.org/) & Derivates
* [OpenSuse](https://www.opensuse.org/) & Derivates
* [ArchLinux](https://www.archlinux.org/) & Derivates
* Eventually also on [FreeBSD](https://www.freebsd.org/) or [OpenBSD](https://www.openbsd.org/)

I am working on geting this to work. I don't care too much on the looks or feel, as I'm looking at using the Symfony Framework for the planned ["v2"](https://github.com/hyperclock/hostkit) version of this.
