#!/usr/bin/php -q
<?php
require_once './conf_inc.php';
require_once './errors_inc.php';

error_reporting($error_reporting);

if($enable_awstats === "on") {
    ($GLOBALS["___mysqli_ston"] = mysqli_connect($hostname,  $admin,  $password_sql)) or die($error_connectdb);
    mysqli_select_db($GLOBALS["___mysqli_ston"], $database) or die($error_selectdb);

    $query = "select domain from domains where user_id!='1';";
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $query) or die($error_select);

    if(mysqli_num_rows($result) != 0) {
        while($row = mysqli_fetch_array($result)) {
            system("$awstats_update -config=$row[domain] -update");
        }
    }
}
?>
